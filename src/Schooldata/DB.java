package Schooldata;
import java.sql.*;

public class DB {
	
	Connection conn;
	Statement stat;
	ResultSet rset;
	
	
		// AWS informtaion
		private final String DB_NAME = "School"; 				//schema name
		private final String DB_USERNAME = "nkmin96";
		private final String DB_PASSWORD = "1234567890";
		private final String DB_HOSTNAME = "mindbinstance.czxpsordr8bq.us-east-2.rds.amazonaws.com"; 	//Endpoint
		private final String DB_PORT = "3306";

		
		String driverName = "com.mysql.jdbc.Driver";
		String jdbcUrl="jdbc:mysql://" + DB_HOSTNAME + ":" + DB_PORT + "/" + DB_NAME + "?user=" + DB_USERNAME 
				+ "&password=" + DB_PASSWORD;

		
		public DB() {
			try {
				Class.forName(driverName);						//JDBC LOAD
			}	
			catch (ClassNotFoundException e) 
			{
				System.out.println(e.getStackTrace());
			}
		}
		public void closeDatabase() {
			try {
				if(conn != null) {
					conn.close();
				}
				if(stat != null) {
					stat.close();
				}
				if(rset != null) {
					rset.close();
				}
			} catch (SQLException e) {
				System.out.println(e.getStackTrace());
			}
			
		}
		
		//all school name
		
		public void LoadSchoolname() {
			try 
			{
				String query = "select * from repository;"; 
				 
				conn = DriverManager.getConnection(jdbcUrl);					 // DB connection
				stat = conn.createStatement(); 									 // DB connection
				rset = stat.executeQuery(query);								 // Excute query
				
				ResultSetMetaData resultSetMetaData = rset.getMetaData();		 // Retrieve column data
				System.out.println(resultSetMetaData.getColumnName(2));			 // Print out the coulumn data
				
				while(rset.next()) {
					System.out.println(rset.getString("schoolname"));
				}
			}
			
				
				catch(SQLException e) {
					System.out.println("query failed"+e.getStackTrace());
				}
				finally {
					closeDatabase();
				}
				
		
		}
		
		//all school name per each section
		public void LoadSectionType(int sectiontype) {
			
			try {
				
				String query="select * from repository where sectiontype =" + sectiontype;
				
				conn = DriverManager.getConnection(jdbcUrl);
				stat = conn.createStatement();
				rset = stat.executeQuery(query);
				
				ResultSetMetaData resultSetMetaData = rset.getMetaData();
				System.out.println(resultSetMetaData.getColumnName(2));
				
				while (rset.next())
			      {
					System.out.println(rset.getString("schoolname"));
				//	System.out.println(rset.getString("sectiontype"));
					
					
			      }
			}
			
			catch(SQLException e) {
				System.out.println("query failed"+e.getStackTrace());
			}
			finally {
				closeDatabase(); 
				}
			}
		
		// all school name per each region
		public void LoadRegionType(int Regiontype) {
			
			try {
				
				String query="select * from repository where Regiontype =" + Regiontype;
				
				conn = DriverManager.getConnection(jdbcUrl);
				stat = conn.createStatement();
				rset = stat.executeQuery(query);
				
				ResultSetMetaData resultSetMetaData = rset.getMetaData();
				System.out.println(resultSetMetaData.getColumnName(2));
				
				while (rset.next())
			      {
					System.out.println(rset.getString("schoolname"));
				//	System.out.println(rset.getString("sectiontype"));
						
			      } 
			}
				catch(SQLException e) {
					System.out.println("query failed"+e.getStackTrace());
				}
				finally {
					closeDatabase(); 
					}
				}
		
		// all school name per each semi-state
		public void LoadSemiStateType(int semistatetype) {
			
			try {
				
				String query="select * from repository where semistatetype =" + semistatetype;
				
				conn = DriverManager.getConnection(jdbcUrl);
				stat = conn.createStatement();
				rset = stat.executeQuery(query);
				
				ResultSetMetaData resultSetMetaData = rset.getMetaData();
				System.out.println(resultSetMetaData.getColumnName(2));
				
				while (rset.next())
			      {
					System.out.println(rset.getString("schoolname"));
				//	System.out.println(rset.getString("sectiontype"));
						
			      } 
			}
				catch(SQLException e) {
					System.out.println("query failed"+e.getStackTrace());
				}
				finally {
					closeDatabase(); 
					}
				}	
		
		
		// all school name per each state
		public void LoadStateType(int statetype) {
			
			try {
				
				String query="select * from repository where statetype =" + statetype;
				
				conn = DriverManager.getConnection(jdbcUrl);
				stat = conn.createStatement();
				rset = stat.executeQuery(query);
				
				ResultSetMetaData resultSetMetaData = rset.getMetaData();
				System.out.println(resultSetMetaData.getColumnName(2));
				
				while (rset.next())
			      {
					System.out.println(rset.getString("schoolname"));
				//	System.out.println(rset.getString("sectiontype"));
						
			      } 
			}
				catch(SQLException e) {
					System.out.println("query failed"+e.getStackTrace());
				}
				finally {
					closeDatabase(); 
					}
				}	
		
		// the last number of section 
		public void NumSectional() {
			
			try {
			String query = "select * from repository order by sectiontype desc limit 1";
			
			conn = DriverManager.getConnection(jdbcUrl);
			stat = conn.createStatement();
			rset = stat.executeQuery(query);
			ResultSetMetaData resultSetMetaData = rset.getMetaData();
		//	System.out.println(resultSetMetaData.getColumnName(3));
			
			
			while (rset.next())
		      {
				
				System.out.println(rset.getString("sectiontype"));
				
		      }
			
			}
			catch(SQLException e) {
				System.out.println("query failed"+e.getStackTrace());
			}
			finally {
				closeDatabase(); 
				}
			
		}
		
		// the last number of region
		public void NumRegional() {
			
			try {
			String query = "select * from repository order by regiontype desc limit 1";
			
			conn = DriverManager.getConnection(jdbcUrl);
			stat = conn.createStatement();
			rset = stat.executeQuery(query);
			ResultSetMetaData resultSetMetaData = rset.getMetaData();
		//	System.out.println(resultSetMetaData.getColumnName(3));
			
			
			while (rset.next())
		      {
				
				System.out.println(rset.getString("regiontype"));
				
		      }
			
			}
			catch(SQLException e) {
				System.out.println("query failed"+e.getStackTrace());
			}
			finally {
				closeDatabase(); 
				}
			
		}
		
		// the last number of semi-state
		public void NumSemiState() {
			
			try {
			String query = "select * from repository order by semistatetype desc limit 1";
			
			conn = DriverManager.getConnection(jdbcUrl);
			stat = conn.createStatement();
			rset = stat.executeQuery(query);
			ResultSetMetaData resultSetMetaData = rset.getMetaData();
		//	System.out.println(resultSetMetaData.getColumnName(3));
			
			
			while (rset.next())
		      {
				
				System.out.println(rset.getString("semistatetype"));
				
		      }
			
			}
			catch(SQLException e) {
				System.out.println("query failed"+e.getStackTrace());
			}
			finally {
				closeDatabase(); 
				}
			
		}
		
		// the last number of state
		public void NumState() {
			
			try {
			String query = "select * from repository order by statetype desc limit 1";
			
			conn = DriverManager.getConnection(jdbcUrl);
			stat = conn.createStatement();
			rset = stat.executeQuery(query);
			ResultSetMetaData resultSetMetaData = rset.getMetaData();
		//	System.out.println(resultSetMetaData.getColumnName(3));
			
			
			while (rset.next())
		      {
				
				System.out.println(rset.getString("statetype"));
				
		      }
			
			}
			catch(SQLException e) {
				System.out.println("query failed"+e.getStackTrace());
			}
			finally {
				closeDatabase(); 
				}
			
		}		
		
		
		public static void main(String[] args) {
			DB datab = new DB();
			
			datab.LoadSchoolname(); //all school name
			datab.LoadSectionType(20); //all school name per each section
			datab.NumSectional();  // the last number of section 
			datab.LoadRegionType(10); // all school name per each region
			datab.LoadSemiStateType(1); // all school name per each semi-state
			datab.LoadStateType(1); // all school name per each state
			datab.NumRegional(); // the last number of region
			datab.NumSemiState(); // the last number of semi-state
			datab.NumState(); // the last number of state
			
			
		}
}
		
		
		